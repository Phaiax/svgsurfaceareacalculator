#!/usr/bin/python
'''
Created on 13.04.2012

This file does only call the cli.py interface.

@author: daniel
'''

import sys

import surfaceareacalculator.calculator.cli

if __name__ == "__main__":
    sys.exit(surfaceareacalculator.calculator.cli.cli())