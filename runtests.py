#!/usr/bin/python

'''
Created on 13.04.2012

@author: daniel
'''
import unittest
from surfaceareacalculator.tests.testswithfiles import TestWithFiles

suite = unittest.TestLoader().loadTestsFromTestCase(TestWithFiles)
unittest.TextTestRunner(verbosity=2).run(suite)