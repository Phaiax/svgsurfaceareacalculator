'''
Created on 13.04.2012

@author: daniel
'''
from errors import (XMLError, XMLNodeNotFoundError, 
    UnsupportedTransformationError, NoPathDataError)
import xml.dom.minidom
import xml.parsers.expat

class SVGFile(object):
    """This class opens and extract paths out of SVG-Files."""
    
    def __init__(self, filename):
        """Opens and parses the SVG file <filename>.
        
        Raises: IOError, XMLError
        
        """
        try:
            self.xmldocument = xml.dom.minidom.parse(filename)
        except xml.parsers.expat.ExpatError as error1:
            raise XMLError(error1)

    def get_node_by_attribute(self, tagname, attribute, value):
        """Gets the XML-Node <tagname attribute="value">."""
        for node in self.xmldocument.getElementsByTagName(tagname):
            if node.getAttribute(attribute) == value:
                return node
        raise XMLNodeNotFoundError(value)
        
    def get_path_node(self, pathid):
        """Gets the XML-Node <path id="pathid">."""
        return self.get_node_by_attribute('path', 'id', pathid)
    
    def get_path_data(self, node):
        """Returns the raw-koordinates data from the attribute d="..".
        
        Raises: NoPathDataError
        
        """
        
        if (hasattr(node, 'hasAttribute') and 
           callable(getattr(node, 'hasAttribute')) and 
           node.hasAttribute("d")):
            
            return node.getAttribute("d")
        raise NoPathDataError(node)
    
    def test_for_transformations(self, node):
        """Searches for unsupported transformations applied to node
        or to parents of node.
        
        Raises: UnsupportedTransformationError
        
        """
    
        # Get id for exception help text.
        nodeid = ""
        if node.hasAttribute("id"):
            nodeid = node.getAttribute("id")
         
        n = node
        while n is not None:
            if (hasattr(n, 'hasAttribute') and 
                callable(getattr(n, 'hasAttribute')) and 
                n.hasAttribute("transform")):
                transform = n.getAttribute("transform")
                for unsupportedtransformation in ["matrix", "scale",
                                                   "skewX", "skewY"]:
                    if transform.find(unsupportedtransformation) != -1:
                        raise UnsupportedTransformationError(
                                  unsupportedtransformation, nodeid)
            n = n.parentNode
        
            
        
        