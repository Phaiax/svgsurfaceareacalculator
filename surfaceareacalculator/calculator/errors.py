#!/usr/bin/python
'''
Created on 13.04.2012

@author: daniel
'''

class SurfaceCalculatorError(Exception):
    """Base Class for Errors in the surfacecalculator package."""
    
    
class XMLError(SurfaceCalculatorError):
    """Generic Error for common XML-Errors."""
    
    def __init(self, errormsg):
        self.errormsg = errormsg
    
    
class XMLNodeNotFoundError(SurfaceCalculatorError):
    """Node could not be found in the document."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid


class NoPathDataError(SurfaceCalculatorError):
    """The path does not contain any coordinates."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid
  
class PathDataError(SurfaceCalculatorError):
    """The path data does contain unsupported modifications. Only straight
    lines are allowed."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid

class PathValueError(SurfaceCalculatorError):
    """The path coordinate values are invalid."""
    
    def __init__(self, nodeid, value):
        self.nodeid = nodeid
        self.value = value


class PathNotClosed(SurfaceCalculatorError):
    """The path is not closed."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid
        
        
class UnsupportedTransformationError(SurfaceCalculatorError):
    """There are unsupported transformations applied to a path or his parents."""
    
    def __init__(self, unsupportedtransformation, nodeid):
        self.unsupportedtransformation = unsupportedtransformation
        self.nodeid = nodeid
