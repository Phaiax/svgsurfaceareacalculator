'''
Created on 12.04.2012

@author: daniel
'''
from svgfile import SVGFile
from svgpath import SVGPath



def begin_calculation(filename, pathname, conversion_pathname=None,
                      factor=1.0, verbose=False, cli=False):
    """Starts the Calculation. This is the main()-Funktion for library-use.
    
    Raises: IOError, XMLError, XMLNodeNotFoundError,
            UnsupportedTransformationError, NoPathDataError, PathValueError
    
    """
    
    svg = SVGFile(filename)
    
    if factor == 1.0 and conversion_pathname:
        # The script should calculate the factor by assuming that the
        # path with the name <conversion_pathname> has the length of 1 meter
        
        conversionnode = svg.get_path_node(conversion_pathname)
        svg.test_for_transformations(conversionnode)
        
        conversion_path = SVGPath(svg.get_path_data(conversionnode), 
                                  conversion_pathname)
        
        length = conversion_path.get_length()
        factor = length
        
            
    # Calculate and print
    pathnode = svg.get_path_node(pathname)
    svg.test_for_transformations(pathnode)
    
    path = SVGPath(svg.get_path_data(pathnode), pathname,
                   require_closed_path = True)
    path.apply_factor(factor)
    surface_area = path.calculate_surface_area()
    
    
    return {"surface_area" : surface_area,
            "factor" : factor,
            "path" : path,
            "pathnode" : pathnode,
            "svg" : svg}



        
        
        
        
        
