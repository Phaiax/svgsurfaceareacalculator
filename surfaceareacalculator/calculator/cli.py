#!/usr/bin/python
'''
Created on 12.04.2012

@author: daniel
'''
from errors import (PathDataError, NoPathDataError, PathValueError, 
    PathNotClosed, UnsupportedTransformationError, XMLError, 
    XMLNodeNotFoundError)
from main import begin_calculation
import argparse
import sys


def cli():
    """Parse commandline arguments and call main()."""

    parser = init_arg_parser()
    args = vars(parser.parse_args())
    calculated_values = calculation(args)
    if calculated_values:
        # Print results
        if args["verbose"]:
            print "The factor value is", calculated_values["factor"]
            print "The surface area is", calculated_values["surface_area"]
        else:
            print calculated_values["surface_area"]


def init_arg_parser():
    """Initializes the argparse module with the parameters and the
    corresponding help texts."""
    parser = argparse.ArgumentParser(description=
        "Calculate the surface of a closed and straight path in a SVG-File. " +
        "Be aware of the fact that this script does not take care of " +
        "path-translations, eg: matrix, scale etc.",
        epilog="You need either the --conversion-example-path or the " + 
        "--factor argument to get usable results. If none of these " + 
        "arguments are given, --factor 1 is used.")
    
    parser.add_argument('--filename', '-f', required=True,
                        help='SVG-file to use.')
    parser.add_argument('--pathname', '-p', required=True,
                        help='The name of the closed and straight ' +  
                        'path the surface area should be calculated of.')
    parser.add_argument('--factor', type=float, default=1,
                        help='Conversion factor between meters and ' + 
                        'inkscape-units.')
    parser.add_argument('--conversion-example-path', '-c', nargs='?', 
                        dest='conversion_pathname',
                        help='Name of a straight path that is used to ' + 
                        'calculate the conversion factor. The length '+ 
                        'of the path should be 1 meter.')
    parser.add_argument('--verbose', '-v', action='store_true', 
                        help='Be verbose.')
    return parser

def calculation(args):
    """Calculates the surface area with the main.begin_calculation()-Method
    and print error messages on error.
    
    Returns: Dictionary from main.begin_calculation (see main.py).
    
    """
    
    calculated_values = {}
    try:
        calculated_values = begin_calculation(cli=True, **args)
    except IOError as (_ , error):
        print "I/O error:", error
        print "Check the filename and the file permissions."
    except XMLNodeNotFoundError as error:
        print XMLNodeNotFoundError.__doc__, "(pathid: ", error.nodeid, ")"
    except XMLError as error:
        print "Could not parse the file. You must use a valid SVG-File ", \
              "for example from inkscape",  "(" , str(error) , ")"
    except UnsupportedTransformationError as error:
        print UnsupportedTransformationError.__doc__, "(unsupported ", \
              "transformation: ", error.unsupportedtransformation, \
              ", nodeid/pathid: ", error.nodeid, ")"
    except PathDataError as error:
        print PathDataError.__doc__, "(pathid: ", error.nodeid, ")"
    except NoPathDataError as error:
        print NoPathDataError.__doc__, "(pathid: ", error.nodeid, ")"
    except PathValueError as error:
        print PathValueError.__doc__, "(pathid: ", error.nodeid, ", value:", \
              error.value, ")"
    except PathNotClosed as error:
        print PathNotClosed.__doc__, "(pathid: ", error.nodeid, ")"
    return calculated_values


if __name__ == "__main__":
    sys.exit(cli())
