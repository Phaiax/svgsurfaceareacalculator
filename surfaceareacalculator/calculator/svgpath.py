'''
Created on 13.04.2012

@author: daniel
'''
from errors import (PathDataError, PathNotClosed, PathValueError,
    NoPathDataError)
import math

class SVGPath(object):
    """This class parses the coordinates-data from a svg-path.
    
    Parse path
        Only a particular kind of paths are allowed.
        The paths have to start with 'm' and end with 'z'.
        m: first coordinates-couple are absolute, the following are in each
           case relative to the previous coordinates-couple, that means they
           are vectors.
        z: indicates closed path. think line to first point
        There must not be other chars!
    
    Example:
        m 56214.989,-9978.506
        -2474.874,-2474.874 -4560.838,0 -3678.861,-3678.861 -9650.102,0
        -2746.032,4756.266 -7592.471,0 -1813.979,-2763.979 -9484.986,0 z   
    """
    
    
    def __init__(self, rawcoordinates, pathid, require_closed_path=False):
        """
        
        Raises: PathDataError, PathValueError
        
        """
        self.rawcoordinates = rawcoordinates.strip()
        self.rawcoordinates_numbers_only = ""
        
        self.pathid = pathid
        self.coordinates = []
        
        self.validate()
        self.parse_raw_coordinates()
        
        if not self.is_closed and require_closed_path:
            raise PathNotClosed(self.pathid)


                
    def validate(self):
        """Check for chars in the coordinates-raw-string. Chars (excluding 
        leading m and trailing z) indicate Path-Transformations like bezier
        curves and are forbidden.
        
        Raises: PathDataError
        
        """
        
        if len(self.rawcoordinates) < 3:
            raise NoPathDataError(self.pathid) 
        
        if not self.rawcoordinates.startswith('m'):
            raise PathDataError(self.pathid)
        self.is_closed = self.rawcoordinates.endswith('z')
        

        if self.is_closed:
            # remove m and z
            self.rawcoordinates_numbers_only = self.rawcoordinates[1:-1].strip() 
        else:
            # remove m
            self.rawcoordinates_numbers_only = self.rawcoordinates[1:].strip() 


        for char in 'abcdefghijklmnopqrstuvwxyz':
            if (self.rawcoordinates_numbers_only.find(char) != -1 or 
               self.rawcoordinates_numbers_only.find(char.upper()) != -1):
                raise PathDataError(self.pathid)
                
    def parse_raw_coordinates(self):
        """This function parses the raw string into a list of 2-list.
        
        raises: PathValueError"""
        
        # The Couples are split by spaces.
        coordinatecouples_raw = self.rawcoordinates_numbers_only.split()
        for coordinatecouple_raw in coordinatecouples_raw:
            # The x and y-coordinates are seperated by ','.
            couple_str = coordinatecouple_raw.split(",")
            try:
                couple = [ float(couple_str[0]), float(couple_str[1]) ]
            except ValueError:
                raise PathValueError(self.pathid, couple_str)
            self.coordinates.append(couple)

    def get_length(self):
        """Returns the length of the path."""
        self.length = 0;
        # Remember the total vector for the closing part of the path.
        x = 0.0
        y = 0.0
        # Don't use the first coordinates, they are absolute.
        for index in range(1, len(self.coordinates)):
            vector = self.coordinates[index]
            self.length = (self.length + 
                math.sqrt(vector[0] * vector[0] + vector[1] * vector[1]))
            x = x + vector[0]
            y = y + vector[1]
            
        if self.is_closed:
            self.length = self.length + math.sqrt(x * x + y * y)
                
        return self.length
        
    def apply_factor(self, factor):
        """Apply factor by effectivly scale every vector."""
        self.coordinates = [ [couple[0] / factor, couple[1]/ factor] \
                                    for couple in self.coordinates ]

    def calculate_surface_area(self):
        """This function does the calculation of the surface area. It uses
        the gaussian trapezium formula."""
        
        # Set the first vector to zero because the offset is irrelevant.
        self.coordinates[0] = [0, 0]
        
        # Calculate absolute coordinates for each vector.
        for index in range(1, len(self.coordinates)):
            self.coordinates[index] = [
                self.coordinates[index - 1][0] + self.coordinates[index][0],
                self.coordinates[index - 1][1] + self.coordinates[index][1]]
        
        
        # Calculcation
        surface = 0
        
        for index in range(0, len(self.coordinates)):
            index_next = index + 1
            # if i > n : i = i % n
            if index > (len(self.coordinates) - 1):
                index = index % len(self.coordinates)
            if index_next > (len(self.coordinates) - 1):
                index_next = index_next % len(self.coordinates)
            surface = (surface + 
              ( self.coordinates[index][1] + self.coordinates[index_next][1] ) *
              ( self.coordinates[index][0] - self.coordinates[index_next][0] ))
        
        # As needed by formula, take the absolute half value.
        self.surface = math.fabs(surface / 2)
        return self.surface
        
        
        