from ..calculator.errors import XMLError, XMLNodeNotFoundError, NoPathDataError, \
    PathNotClosed, PathDataError, UnsupportedTransformationError
from ..calculator.main import begin_calculation
from ..calculator.svgfile import SVGFile
from ..calculator.svgpath import SVGPath
from xml.dom.minidom import Node, Document
import subprocess
import unittest

class TestWithFiles(unittest.TestCase):
    
    basePath = "surfaceareacalculator/tests/SVGTestFiles/"
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass
    
    def test_1sqm(self):
        results = begin_calculation(
                          filename=self.basePath + "1sqm_as_path.svg",
                          pathname="path2989", conversion_pathname="path3159")
        
        self.assertTrue(results["surface_area"] > 0.95, 'Error in calculation')
        self.assertTrue(results["surface_area"] < 1.05, 'Error in calculation')
        
    def test_results_exists(self):
        results = begin_calculation(
                          filename=self.basePath + "1sqm_as_path.svg",
                          pathname="path2989", conversion_pathname="path3159")
        
        self.assertIsInstance(results["surface_area"], float)
        self.assertIsInstance(results["factor"], float)
        self.assertIsInstance(results["pathnode"], Node)
        self.assertIsInstance(results["path"], SVGPath)
        self.assertIsInstance(results["svg"], SVGFile)
        self.assertIsInstance(results["svg"].xmldocument, Document)        

        self.assertGreater(len(results["path"].coordinates), 0,
                           'No coordinates recognized')
        
    def test_equal_and_closed_conversion_path(self):
        """ Test math with equal paths.
        
        In the test file the conversion path is just a copy of the
        closed path for calculation. Because the length of the one meter
        equivalent is the same as the complete outline, the surface area
        should be A ~= (1/4)^2 = 1/16. """
        
        results = begin_calculation(
                          filename=self.basePath + "1sqm_two_times.svg",
                          pathname="path2989", conversion_pathname="path2989-8")
    
        self.assertAlmostEqual(1.0/16, results["surface_area"], delta=0.005)
    

        # Same should work when using the same path instead of equal paths
        results = begin_calculation(
                          filename=self.basePath + "1sqm_as_path.svg",
                          pathname="path2989", conversion_pathname="path2989")
    
        self.assertAlmostEqual(1.0/16, results["surface_area"], delta=0.005)

    def test_invalid_xml_1(self):
        """The tested file contains an invalid XML-root tag."""
        self.assertRaises(XMLError, begin_calculation, 
                          filename=self.basePath + "invalid_xml_1.svg",
                          pathname="path2989", conversion_pathname="path2989")
        
    
    def test_invalid_svg_no_path_id(self):
        """In the tested file, there is a <path> without id attribute."""
        self.assertRaises(XMLNodeNotFoundError, begin_calculation, 
                          filename=self.basePath + "invalid_svg_no_path_id.svg",
                          pathname="path2989", conversion_pathname="path2989")
        
        
    def test_no_path(self):
        """In the tested file, there is no path present."""
        self.assertRaises(XMLNodeNotFoundError, begin_calculation, 
                          filename=self.basePath + "no_path.svg",
                          pathname="path2989", conversion_pathname="path2989")
       
        
    def test_no_coordinates(self):
        """In the tested file, there is a path with an empty d-Attribute."""
        self.assertRaises(NoPathDataError, begin_calculation, 
                          filename=self.basePath + "no_coordinates.svg",
                          pathname="path2989", conversion_pathname="path2989")
         
        
        
    def test_unclosed_path(self):
        """In the tested file, there is unclosed Path."""
        self.assertRaises(PathNotClosed, begin_calculation, 
                          filename=self.basePath + "unclosed_path.svg",
                          pathname="path3005")
        
 
    def test_absolute_coordinates(self):
        """In the tested file are used absolute coordinates."""
        self.assertRaises(PathDataError, begin_calculation, 
                          filename=self.basePath + "absolute_coordinates.svg",
                          pathname="path3029")

    def test_transformation_detection(self):
        """In the tested file, a matrix transformation is applied
        to path3029."""
        
        self.assertRaises(UnsupportedTransformationError, begin_calculation, 
                          filename=self.basePath + "matrix_transformation.svg",
                          pathname="path3029")
        
    def test_complicated_file(self):
        """Test with a more complicated file."""
        results = begin_calculation(
                          filename=self.basePath + "test.svg",
                          pathname="path15471", conversion_pathname="path8115")
        self.assertAlmostEqual(results["surface_area"], 62.4117498847)
        
    def test_complicated_file_via_cli(self):
        """Test the CLI-Interface via system-call."""
        process = subprocess.Popen(
                          ["./surfaceareacalculator/calculator/cli.py -f" +
                          self.basePath + "test.svg -p path15471 -c path8115"],
                          shell=True, stdout=subprocess.PIPE)
        result = process.communicate()
        self.assertTrue(result[0].startswith("62.41174"))
 
    
    