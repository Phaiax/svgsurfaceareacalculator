#!/usr/bin/python
'''
Created on 13.04.2012

@author: daniel
'''

class SurfaceCalculatorError(Exception):
    """Base Class for Errors in the surfacecalculator package."""
    
    
class XMLError(SurfaceCalculatorError):
    """Generic Error for common XML-Errors."""
    
    def __init(self, errormsg):
        self.errormsg = errormsg
    
    
class XMLNodeNotFoundError(SurfaceCalculatorError):
    """Node could not be found in the document."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid


class NoPathDataError(SurfaceCalculatorError):
    """The path does not contain any coordinates."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid
  
class PathDataError(SurfaceCalculatorError):
    """The path data does contain unsupported modifications. Only straight
    lines are allowed."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid

class PathValueError(SurfaceCalculatorError):
    """The path coordinate values are invalid."""
    
    def __init__(self, nodeid, value):
        self.nodeid = nodeid
        self.value = value


class PathNotClosed(SurfaceCalculatorError):
    """The path is not closed."""
    
    def __init__(self, nodeid):
        self.nodeid = nodeid
        
        
class UnsupportedTransformationError(SurfaceCalculatorError):
    """There are unsupported transformations applied to a path or his parents."""
    
    def __init__(self, unsupportedtransformation, nodeid):
        self.unsupportedtransformation = unsupportedtransformation
        self.nodeid = nodeid
'''
Created on 13.04.2012

@author: daniel
'''
import argparse
import math
import sys
import xml.dom.minidom
import xml.parsers.expat

class SVGFile():
    """This class opens and extract paths out of SVG-Files."""
    
    def __init__(self, filename):
        """Opens and parses the SVG file <filename>.
        
        Raises: IOError, XMLError
        
        """
        try:
            self.xmldocument = xml.dom.minidom.parse(filename)
        except xml.parsers.expat.ExpatError as error1:
            raise XMLError(error1)

    def get_node_by_attribute(self, tagname, attribute, value):
        """Gets the XML-Node <tagname attribute="value">."""
        for node in self.xmldocument.getElementsByTagName(tagname):
            if node.getAttribute(attribute) == value:
                return node
        raise XMLNodeNotFoundError(value)
        
    def get_path_node(self, pathid):
        """Gets the XML-Node <path id="pathid">."""
        return self.get_node_by_attribute('path', 'id', pathid)
    
    def get_path_data(self, node):
        """Returns the raw-koordinates data from the attribute d="..".
        
        Raises: NoPathDataError
        
        """
        
        if (hasattr(node, 'hasAttribute') and 
           callable(getattr(node, 'hasAttribute')) and 
           node.hasAttribute("d")):
            
            return node.getAttribute("d")
        raise NoPathDataError(node)
    
    def test_for_transformations(self, node):
        """Searches for unsupported transformations applied to node
        or to parents of node.
        
        Raises: UnsupportedTransformationError
        
        """
    
        # Get id for exception help text.
        nodeid = ""
        if node.hasAttribute("id"):
            nodeid = node.getAttribute("id")
         
        n = node
        while n is not None:
            if (hasattr(n, 'hasAttribute') and 
                callable(getattr(n, 'hasAttribute')) and 
                n.hasAttribute("transform")):
                transform = n.getAttribute("transform")
                for unsupportedtransformation in ["matrix", "scale",
                                                   "skewX", "skewY"]:
                    if transform.find(unsupportedtransformation) != -1:
                        raise UnsupportedTransformationError(
                                  unsupportedtransformation, nodeid)
            n = n.parentNode
        
            
        
        '''
Created on 13.04.2012

@author: daniel
'''

class SVGPath():
    """This class parses the coordinates-data from a svg-path.
    
    Parse path
        Only a particular kind of paths are allowed.
        The paths have to start with 'm' and end with 'z'.
        m: first coordinates-couple are absolute, the following are in each
           case relative to the previous coordinates-couple, that means they
           are vectors.
        z: indicates closed path. think line to first point
        There must not be other chars!
    
    Example:
        m 56214.989,-9978.506
        -2474.874,-2474.874 -4560.838,0 -3678.861,-3678.861 -9650.102,0
        -2746.032,4756.266 -7592.471,0 -1813.979,-2763.979 -9484.986,0 z   
    """
    
    
    def __init__(self, rawcoordinates, pathid, require_closed_path=False):
        """
        
        Raises: PathDataError, PathValueError
        
        """
        self.rawcoordinates = rawcoordinates.strip()
        self.rawcoordinates_numbers_only = ""
        
        self.pathid = pathid
        self.coordinates = []
        
        self.validate()
        self.parse_raw_coordinates()
        
        if not self.is_closed and require_closed_path:
            raise PathNotClosed(self.pathid)


                
    def validate(self):
        """Check for chars in the coordinates-raw-string. Chars (excluding 
        leading m and trailing z) indicate Path-Transformations like bezier
        curves and are forbidden.
        
        Raises: PathDataError
        
        """
        
        if len(self.rawcoordinates) < 3:
            raise NoPathDataError(self.pathid) 
        
        if not self.rawcoordinates.startswith('m'):
            raise PathDataError(self.pathid)
        self.is_closed = self.rawcoordinates.endswith('z')
        

        if self.is_closed:
            # remove m and z
            self.rawcoordinates_numbers_only = self.rawcoordinates[1:-1].strip() 
        else:
            # remove m
            self.rawcoordinates_numbers_only = self.rawcoordinates[1:].strip() 


        for char in 'abcdefghijklmnopqrstuvwxyz':
            if (self.rawcoordinates_numbers_only.find(char) != -1 or 
               self.rawcoordinates_numbers_only.find(char.upper()) != -1):
                raise PathDataError(self.pathid)
                
    def parse_raw_coordinates(self):
        """This function parses the raw string into a list of 2-list.
        
        raises: PathValueError"""
        
        # The Couples are split by spaces.
        coordinatecouples_raw = self.rawcoordinates_numbers_only.split()
        for coordinatecouple_raw in coordinatecouples_raw:
            # The x and y-coordinates are seperated by ','.
            couple_str = coordinatecouple_raw.split(",")
            try:
                couple = [ float(couple_str[0]), float(couple_str[1]) ]
            except ValueError:
                raise PathValueError(self.pathid, couple_str)
            self.coordinates.append(couple)

    def get_length(self):
        """Returns the length of the path."""
        self.length = 0;
        # Remember the total vector for the closing part of the path.
        x = 0.0
        y = 0.0
        # Don't use the first coordinates, they are absolute.
        for index in range(1, len(self.coordinates)):
            vector = self.coordinates[index]
            self.length = (self.length + 
                math.sqrt(vector[0] * vector[0] + vector[1] * vector[1]))
            x = x + vector[0]
            y = y + vector[1]
            
        if self.is_closed:
            self.length = self.length + math.sqrt(x * x + y * y)
                
        return self.length
        
    def apply_factor(self, factor):
        """Apply factor by effectivly scale every vector."""
        self.coordinates = [ [couple[0] / factor, couple[1]/ factor] \
                                    for couple in self.coordinates ]

    def calculate_surface_area(self):
        """This function does the calculation of the surface area. It uses
        the gaussian trapezium formula."""
        
        # Set the first vector to zero because the offset is irrelevant.
        self.coordinates[0] = [0, 0]
        
        # Calculate absolute coordinates for each vector.
        for index in range(1, len(self.coordinates)):
            self.coordinates[index] = [
                self.coordinates[index - 1][0] + self.coordinates[index][0],
                self.coordinates[index - 1][1] + self.coordinates[index][1]]
        
        
        # Calculcation
        surface = 0
        
        for index in range(0, len(self.coordinates)):
            index_next = index + 1
            # if i > n : i = i % n
            if index > (len(self.coordinates) - 1):
                index = index % len(self.coordinates)
            if index_next > (len(self.coordinates) - 1):
                index_next = index_next % len(self.coordinates)
            surface = (surface + 
              ( self.coordinates[index][1] + self.coordinates[index_next][1] ) *
              ( self.coordinates[index][0] - self.coordinates[index_next][0] ))
        
        # As needed by formula, take the absolute half value.
        self.surface = math.fabs(surface / 2)
        return self.surface
        
        
        '''
Created on 12.04.2012

@author: daniel
'''



def begin_calculation(filename, pathname, conversion_pathname=None,
                      factor=1.0, verbose=False, cli=False):
    """Starts the Calculation. This is the main()-Funktion for library-use.
    
    Raises: IOError, XMLError, XMLNodeNotFoundError,
            UnsupportedTransformationError, NoPathDataError, PathValueError
    
    """
    
    svg = SVGFile(filename)
    
    if factor == 1.0 and conversion_pathname:
        # The script should calculate the factor by assuming that the
        # path with the name <conversion_pathname> has the length of 1 meter
        
        conversionnode = svg.get_path_node(conversion_pathname)
        svg.test_for_transformations(conversionnode)
        
        conversion_path = SVGPath(svg.get_path_data(conversionnode), 
                                  conversion_pathname)
        
        length = conversion_path.get_length()
        factor = length
        
            
    # Calculate and print
    pathnode = svg.get_path_node(pathname)
    svg.test_for_transformations(pathnode)
    
    path = SVGPath(svg.get_path_data(pathnode), pathname,
                   require_closed_path = True)
    path.apply_factor(factor)
    surface_area = path.calculate_surface_area()
    
    
    return {"surface_area" : surface_area,
            "factor" : factor,
            "path" : path,
            "pathnode" : pathnode,
            "svg" : svg}



        
        
        
        
        
#!/usr/bin/python
'''
Created on 12.04.2012

@author: daniel
'''


def cli():
    """Parse commandline arguments and call main()."""

    parser = init_arg_parser()
    args = vars(parser.parse_args())
    calculated_values = calculation(args)
    if calculated_values:
        # Print results
        if args["verbose"]:
            print "The factor value is", calculated_values["factor"]
            print "The surface area is", calculated_values["surface_area"]
        else:
            print calculated_values["surface_area"]


def init_arg_parser():
    """Initializes the argparse module with the parameters and the
    corresponding help texts."""
    parser = argparse.ArgumentParser(description=
        "Calculate the surface of a closed and straight path in a SVG-File. " +
        "Be aware of the fact that this script does not take care of " +
        "path-translations, eg: matrix, scale etc.",
        epilog="You need either the --conversion-example-path or the " + 
        "--factor argument to get usable results. If none of these " + 
        "arguments are given, --factor 1 is used.")
    
    parser.add_argument('--filename', '-f', required=True,
                        help='SVG-file to use.')
    parser.add_argument('--pathname', '-p', required=True,
                        help='The name of the closed and straight ' +  
                        'path the surface area should be calculated of.')
    parser.add_argument('--factor', type=float, default=1,
                        help='Conversion factor between meters and ' + 
                        'inkscape-units.')
    parser.add_argument('--conversion-example-path', '-c', nargs='?', 
                        dest='conversion_pathname',
                        help='Name of a straight path that is used to ' + 
                        'calculate the conversion factor. The length '+ 
                        'of the path should be 1 meter.')
    parser.add_argument('--verbose', '-v', action='store_true', 
                        help='Be verbose.')
    return parser

def calculation(args):
    """Calculates the surface area with the main.begin_calculation()-Method
    and print error messages on error.
    
    Returns: Dictionary from main.begin_calculation (see main.py).
    
    """
    
    calculated_values = {}
    try:
        calculated_values = begin_calculation(cli=True, **args)
    except IOError as (_ , error):
        print "I/O error:", error
        print "Check the filename and the file permissions."
    except XMLNodeNotFoundError as error:
        print XMLNodeNotFoundError.__doc__, "(pathid: ", error.nodeid, ")"
    except XMLError as error:
        print "Could not parse the file. You must use a valid SVG-File ", \
              "for example from inkscape",  "(" , str(error) , ")"
    except UnsupportedTransformationError as error:
        print UnsupportedTransformationError.__doc__, "(unsupported ", \
              "transformation: ", error.unsupportedtransformation, \
              ", nodeid/pathid: ", error.nodeid, ")"
    except PathDataError as error:
        print PathDataError.__doc__, "(pathid: ", error.nodeid, ")"
    except NoPathDataError as error:
        print NoPathDataError.__doc__, "(pathid: ", error.nodeid, ")"
    except PathValueError as error:
        print PathValueError.__doc__, "(pathid: ", error.nodeid, ", value:", \
              error.value, ")"
    except PathNotClosed as error:
        print PathNotClosed.__doc__, "(pathid: ", error.nodeid, ")"
    return calculated_values


if __name__ == "__main__":
    sys.exit(cli())
